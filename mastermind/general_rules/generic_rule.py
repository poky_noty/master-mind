import abc

class GenericRule(object):
    __metaclass__ = abc.ABCMeta 

    def __init__(self, collected_data, stored_data):
        self.stored_data = stored_data
        self.collected_data = collected_data

    @abc.abstractmethod
    def apply(self):
        raise Exception( "Not Yet Implemented Exception" )

