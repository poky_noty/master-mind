from .comparison_rules_engine import ComparisonRulesEngine 

import logging
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

class ValidateCollectedData(object):
    def __init__(self, collected_data, stored_data, rule_name):
        self.collected_data = collected_data
        self.stored_data = stored_data
        self.rule_name = rule_name

    def compare_collected_stored(self):
        # Collected-Data is type of .. HostDiscoveryBook
        # Stored-Data is type of ... python:list
        logger.debug( "\nCollected Data Type is ... %s\nStored Data Type is ... %s", 
            type(self.collected_data), type(self.stored_data) )

        self.apply_requested_rule()

    def apply_requested_rule(self):
        # Depending on selected rule for datasets comparison ... create an appropriate rule-engine
        engine = ComparisonRulesEngine(self.rule_name)
        rule_class = engine.select_rule_engine()

        # Initialize Rule-Engine with collected and stored datasets
        rule_obj = rule_class( collected_data = self.collected_data, stored_data = self.stored_data )
        
        # Apply rule and check whether the datasets satisfy imposed requirements 
        rule_obj.apply()


        
        